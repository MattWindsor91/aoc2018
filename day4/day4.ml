(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

let with_records claims =
  let open Or_error.Let_syntax in
  let%bind part_one_result = Part1.run claims in
  Out_channel.printf "Part 1: %d\n" part_one_result;
  let%map part_two_result = Part2.run claims in
  Out_channel.printf "Part 2: %d\n" part_two_result
;;


let get_records file =
  Or_error.(
    try_with (fun () -> In_channel.read_lines file)
    >>= Record.of_string_list
  )
;;

let run file = Or_error.(file |> get_records >>= with_records)
