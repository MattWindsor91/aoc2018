(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Base

(** [run records] runs AOC2018 day 4 part 2 on guard records [records]. *)
val run : Record.t list -> int Or_error.t
