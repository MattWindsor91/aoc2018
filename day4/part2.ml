(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

let on_minute guard_id ~key ~data existing =
  let is_more_frequent =
    Option.value_map existing ~default:true
      ~f:(fun (_id, _minute, frequency) -> frequency < data)
  in if is_more_frequent then Some (guard_id, key, data) else existing
;;

let on_guard ~key ~data existing =
  Map.fold (Analysis.Guard.minute_occurrences data)
    ~init:existing ~f:(on_minute key)
;;

let most_asleep_guard_minute state =
  Result.(
    Map.fold (Analysis.guards state) ~init:None ~f:on_guard
    |> of_option ~error:(Error.of_string "No data?")
    >>| (fun (guard, minute, _) -> (guard, minute))
  )
;;

let%expect_test "most_asleep_guard_minute: example" =
  let result =
    Or_error.(
      Analysis.example
      |>  Analysis.of_string_list
      >>= most_asleep_guard_minute
    )
  in
  Sexp.output_hum Out_channel.stdout [%sexp (result : (int * int) Or_error.t)];
  [%expect {| (Ok (99 45)) |}]
;;

let run records =
  let open Or_error.Let_syntax in
  let%bind state = Analysis.of_records records in
  let%map  (id, minute) = most_asleep_guard_minute state in
  id * minute
;;

let%expect_test "run: example" =
  let result = Or_error.(Analysis.example |> Record.of_string_list >>= run) in
  Sexp.output_hum Out_channel.stdout [%sexp (result : int Or_error.t)];
  [%expect {| (Ok 4455) |}]
;;


