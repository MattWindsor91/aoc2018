(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

let compare_guard_asleepness ~key ~data existing =
  let is_more_asleep =
    Option.value_map existing ~default:true
      ~f:(fun (_id, guard) ->
               Analysis.Guard.total guard
               < Analysis.Guard.total data)
  in if is_more_asleep then Some (key, data) else existing
;;

let most_asleep_guard state =
  Map.fold (Analysis.guards state) ~init:None ~f:compare_guard_asleepness
  |> Result.of_option ~error:(Error.of_string "No guards!")
;;

let%expect_test "most_asleep_guard: example" =
  let result =
    Or_error.(Analysis.example |> Analysis.of_string_list >>= most_asleep_guard)
  in
  Sexp.output_hum Out_channel.stdout
    [%sexp (result : (int * Analysis.Guard.t) Or_error.t)];
  [%expect {|
    (Ok
     (10
      ((minute_occurrences
        ((5 1) (6 1) (7 1) (8 1) (9 1) (10 1) (11 1) (12 1) (13 1) (14 1)
         (15 1) (16 1) (17 1) (18 1) (19 1) (20 1) (21 1) (22 1) (23 1) (24 2)
         (25 1) (26 1) (27 1) (28 1) (30 1) (31 1) (32 1) (33 1) (34 1) (35 1)
         (36 1) (37 1) (38 1) (39 1) (40 1) (41 1) (42 1) (43 1) (44 1) (45 1)
         (46 1) (47 1) (48 1) (49 1) (50 1) (51 1) (52 1) (53 1) (54 1)))
       (total 50)))) |}]
;;

let compare_minute_asleepness ~key ~data (ex_key, ex_data) =
  if ex_data < data then (key, data) else (ex_key, ex_data)
;;

let most_asleep_minute guard =
  Map.fold (Analysis.Guard.minute_occurrences guard)
    ~init:(-1, -1) ~f:compare_minute_asleepness
  |> fst
;;

let%expect_test "most_asleep_minute: example" =
  let result =
    Or_error.(
      Analysis.example
      |>  Analysis.of_string_list
      >>= most_asleep_guard
      >>| snd
      >>| most_asleep_minute
    )
  in
  Sexp.output_hum Out_channel.stdout [%sexp (result : int Or_error.t)];
  [%expect {| (Ok 24) |}]
;;

let run records =
  let open Or_error.Let_syntax in
  let%bind state = Analysis.of_records records in
  let%map  (id, guard) = most_asleep_guard state in
  let      minute      = most_asleep_minute guard in
  id * minute
;;

let%expect_test "run: example" =
  let result = Or_error.(Analysis.example |> Record.of_string_list >>= run) in
  Sexp.output_hum Out_channel.stdout [%sexp (result : int Or_error.t)];
  [%expect {| (Ok 240) |}]
;;


