(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

let minutes_asleep =
  List.range ~stride:1 ~start:`inclusive ~stop:`exclusive
;; 

module Guard = struct
  type t =
    { minute_occurrences : int Int.Map.t
    ; total : int
    }
  [@@deriving sexp, fields]
  ;;

  let rec update min_asleep min_awake = function
    | None ->
      update min_asleep min_awake
        (Some { minute_occurrences = Int.Map.empty
              ; total = 0
              })
    | Some { minute_occurrences; total } ->
      let minutes = minutes_asleep min_asleep min_awake in
      let total' = total + List.length minutes in
      let occs' = List.fold minutes ~init:minute_occurrences
          ~f:(Int.Map.update ~f:(Option.value_map ~default:1 ~f:succ))
      in
      { minute_occurrences = occs'; total = total' }
  ;;
end

type on_duty =
  | No_guard
  | Asleep of { guard : int; min : int }
  | Awake  of int
[@@deriving sexp]
;;

type t =
  { guards : Guard.t Int.Map.t; on_duty : on_duty }
[@@deriving sexp, fields]

let init () = { guards = Int.Map.empty; on_duty = No_guard }

let process_asleep state timestamp =
  let min = Record.Timestamp.minute timestamp in
  match state.on_duty with
  | Awake guard ->
    Or_error.return { state with on_duty = Asleep { guard; min } }
  | Asleep _ | No_guard -> Or_error.error_string "Invalid guard state"
;;

let process_awake state timestamp =
  let open Or_error.Let_syntax in
  match state.on_duty with
  | Asleep { guard; min } ->
    let min' = Record.Timestamp.minute timestamp in
    if min < min' then (
      let guards =
        Int.Map.update state.guards guard ~f:(Guard.update min min')
      in
      return { on_duty = Awake guard; guards }
    ) else Or_error.error_string "Slept a non-positive number of minutes"
  | Awake _ | No_guard -> Or_error.error_string "Invalid guard state"
;;

let add_event state record =
  let timestamp = Record.timestamp record in
  match Record.event record with
  | Record.Event.Begins_shift guard ->
    Or_error.return { state with on_duty = Awake guard }
  | Falls_asleep -> process_asleep state timestamp
  | Wakes_up -> process_awake state timestamp
;;

let time_compare x y =
  Record.Timestamp.compare (Record.timestamp x) (Record.timestamp y)
;;

let of_records records =
  let sorted_records = List.sort ~compare:time_compare records in
  List.fold_result ~init:(init ()) ~f:add_event sorted_records
;;

let example =
  [ "[1518-11-01 00:00] Guard #10 begins shift"
  ; "[1518-11-01 00:05] falls asleep"
  ; "[1518-11-01 00:25] wakes up"
  ; "[1518-11-01 00:30] falls asleep"
  ; "[1518-11-01 00:55] wakes up"
  ; "[1518-11-01 23:58] Guard #99 begins shift"
  ; "[1518-11-02 00:40] falls asleep"
  ; "[1518-11-02 00:50] wakes up"
  ; "[1518-11-03 00:05] Guard #10 begins shift"
  ; "[1518-11-03 00:24] falls asleep"
  ; "[1518-11-03 00:29] wakes up"
  ; "[1518-11-04 00:02] Guard #99 begins shift"
  ; "[1518-11-04 00:36] falls asleep"
  ; "[1518-11-04 00:46] wakes up"
  ; "[1518-11-05 00:03] Guard #99 begins shift"
  ; "[1518-11-05 00:45] falls asleep"
  ; "[1518-11-05 00:55] wakes up"
  ]
;;

let of_string_list strs =
    Or_error.(strs |> Record.of_string_list >>= of_records)
;;

let%expect_test "of_string_list: example" =
  let result = of_string_list example in
  Sexp.output_hum Out_channel.stdout [%sexp (result : t Or_error.t)];
  [%expect {|
    (Ok
     ((guards
       ((10
         ((minute_occurrences
           ((5 1) (6 1) (7 1) (8 1) (9 1) (10 1) (11 1) (12 1) (13 1) (14 1)
            (15 1) (16 1) (17 1) (18 1) (19 1) (20 1) (21 1) (22 1) (23 1)
            (24 2) (25 1) (26 1) (27 1) (28 1) (30 1) (31 1) (32 1) (33 1)
            (34 1) (35 1) (36 1) (37 1) (38 1) (39 1) (40 1) (41 1) (42 1)
            (43 1) (44 1) (45 1) (46 1) (47 1) (48 1) (49 1) (50 1) (51 1)
            (52 1) (53 1) (54 1)))
          (total 50)))
        (99
         ((minute_occurrences
           ((36 1) (37 1) (38 1) (39 1) (40 2) (41 2) (42 2) (43 2) (44 2)
            (45 3) (46 2) (47 2) (48 2) (49 2) (50 1) (51 1) (52 1) (53 1)
            (54 1)))
          (total 30)))))
      (on_duty (Awake 99)))) |}]
;;


