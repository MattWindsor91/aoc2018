(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

module Event = struct
  type t =
    | Begins_shift of int
    | Falls_asleep
    | Wakes_up
  [@@deriving sexp]
  ;;

  let of_string str =
    if String.equal str "falls asleep" then Ok Falls_asleep else
    if String.equal str "wakes up"     then Ok Wakes_up     else
    try
      Caml.Scanf.sscanf str "Guard #%d begins shift" (fun i -> Begins_shift i)
      |> Or_error.return
    with
    | Caml.Scanf.Scan_failure _ | Failure _ | End_of_file ->
      Or_error.error_s [%message "Invalid event syntax" ~got:str]
  ;;

  let%expect_test "of_string: falls asleep" =
    Sexp.output_hum Out_channel.stdout
      [%sexp (of_string "falls asleep" : t Or_error.t)];
    [%expect {| (Ok Falls_asleep) |}]
  ;;

  let%expect_test "of_string: wakes up" =
    Sexp.output_hum Out_channel.stdout
      [%sexp (of_string "wakes up" : t Or_error.t)];
    [%expect {| (Ok Wakes_up) |}]
  ;;

  let%expect_test "of_string: guard begins shift" =
    Sexp.output_hum Out_channel.stdout
      [%sexp (of_string "Guard #32767 begins shift" : t Or_error.t)];
    [%expect {| (Ok (Begins_shift 32767)) |}]
  ;;
end

module Timestamp = struct
  module M = struct
    type t =
      { year   : int
      ; month  : int
      ; day    : int
      ; hour   : int
      ; minute : int
      }
    [@@deriving sexp, fields]
    ;;

    let on f g x y = g (f x) (f y)

    let compare = Comparable.lexicographic
      [ on year   Int.compare
      ; on month  Int.compare
      ; on day    Int.compare
      ; on hour   Int.compare
      ; on minute Int.compare
      ]
    ;;
  end

  include M
  include Comparable.Make_plain (M)

  let create year month day hour minute =
    { year; month; day; hour; minute }

  let of_string str =
    try
      Caml.Scanf.sscanf str "%d-%d-%d %d:%d" create |> Or_error.return
    with
    | Caml.Scanf.Scan_failure _ | Failure _ | End_of_file ->
      Or_error.error_s [%message "Invalid timestamp syntax" ~got:str]
  ;;

  let%expect_test "of_string: example" =
    Sexp.output_hum Out_channel.stdout
      [%sexp (of_string "1518-11-01 00:00" : t Or_error.t)];
    [%expect {| (Ok ((year 1518) (month 11) (day 1) (hour 0) (minute 0))) |}]
  ;;
end

type t =
  { timestamp : Timestamp.t
  ; event     : Event.t
  }
[@@deriving sexp, fields]
;;

let split_timestamp str =
  let open Option.Monad_infix in
  str
  |>  String.strip ~drop:(fun c -> c = '[' || Char.is_whitespace c)
  |>  String.lsplit2 ~on:']'
  >>| Tuple2.map_fst ~f:(String.rstrip ~drop:(Char.is_whitespace))
  >>| Tuple2.map_snd ~f:(String.lstrip ~drop:(Char.is_whitespace))
  |>  Result.of_option
    ~error:(Error.create_s [%message "Invalid timestamp syntax" ~got:str])
;;

let%expect_test "split_timestamp: example" =
  Sexp.output_hum Out_channel.stdout
    [%sexp (split_timestamp "[1518-11-01 00:00] Guard #10 begins shift"
            : (string * string) Or_error.t)];
  [%expect {| (Ok ("1518-11-01 00:00" "Guard #10 begins shift")) |}]
;;

let of_string str =
  let open Or_error.Let_syntax in
  let%bind timestamp_str, event_str = split_timestamp str in
  let%map  timestamp = Timestamp.of_string timestamp_str
  and      event     = Event.of_string event_str in
  { timestamp; event }
;;

let%expect_test "of_string: example" =
  Sexp.output_hum Out_channel.stdout
    [%sexp (of_string "[1518-11-01 00:00] Guard #10 begins shift"
            : t Or_error.t)];
  [%expect {|
    (Ok
     ((timestamp ((year 1518) (month 11) (day 1) (hour 0) (minute 0)))
      (event (Begins_shift 10)))) |}]
;;

let of_string_list strs =
  strs |> List.map ~f:of_string |> Or_error.combine_errors
;;
