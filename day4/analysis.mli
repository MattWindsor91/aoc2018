(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

module Guard : sig
  type t [@@deriving sexp]

  val minute_occurrences : t -> int Int.Map.t
  val total : t -> int
end

type t [@@deriving sexp]

val guards : t -> Guard.t Int.Map.t

val of_records : Record.t list -> t Or_error.t
val of_string_list : string list -> t Or_error.t

val example : string list
