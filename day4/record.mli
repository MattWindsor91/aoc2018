(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

module Event : sig
  type t =
    | Begins_shift of int
    | Falls_asleep
    | Wakes_up
  [@@deriving sexp]
  ;;

  val of_string : string -> t Or_error.t
end

module Timestamp : sig
  type t [@@deriving sexp]

  val year   : t -> int
  val month  : t -> int
  val day    : t -> int
  val hour   : t -> int
  val minute : t -> int

  val of_string : string -> t Or_error.t

  include Comparable.S_plain with type t := t
end

type t [@@deriving sexp]

val event : t -> Event.t
val timestamp : t -> Timestamp.t

val of_string : string -> t Or_error.t
val of_string_list : string list -> t list Or_error.t
