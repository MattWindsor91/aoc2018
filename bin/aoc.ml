(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Base
open Stdio

module type Day = sig
  val run : string -> unit Or_error.t
end

let modules : (module Day) list =
  [ (module Day1)
  ; (module Day2)
  ; (module Day3)
  ; (module Day4)
  ; (module Day5)
  ]
;;

let parse_day sd =
  let od = Caml.int_of_string_opt sd in
  Result.of_option od ~error:(Error.createf "Invalid day argument: %s" sd)
;;

let get_day_number () =
  match Array.to_list Sys.argv with
  | [] | [ _ ] ->
    Or_error.error_string "Expected a day argument, got nothing"
  | [ _name; sd ] -> parse_day sd
  | _ :: _ -> Or_error.error_string "Expected a day argument, got too many"
;;

let get_day_module dnum =
  let mo = List.nth modules (dnum - 1) in
  Result.of_option mo ~error:(Error.createf "Day not found: %d" dnum)
;;

let run () =
  let open Or_error.Let_syntax in
  let%bind dnum = get_day_number () in
  let%bind (module Day) = get_day_module dnum in
  let input_file = Printf.sprintf "day%d/input" dnum in
  Day.run input_file
;;

let () =
  match run () with
  | Ok () -> ()
  | Error e -> Out_channel.eprintf "Error: %s" (Error.to_string_hum e)
;;
