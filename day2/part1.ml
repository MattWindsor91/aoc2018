(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

type single = { two : bool; three : bool } [@@deriving sexp]
type t = { twos : int; threes : int } [@@deriving sexp]

let to_counts box_id =
  box_id
  |> String.to_list
  |> List.sort ~compare:Char.compare
  |> List.group ~break:(fun x y -> not (Char.equal x y))
  |> List.map ~f:List.length
  |> Int.Set.of_list
;;

let classify box_id =
  let counts = to_counts box_id in
  let two = Int.Set.mem counts 2 in
  let three = Int.Set.mem counts 3 in
  { two; three }
;;

let%expect_test "abcdef" =
  Sexp.output_hum Out_channel.stdout [%sexp (classify "abcdef" : single)];
  [%expect {| ((two false) (three false)) |}]
;;

let%expect_test "bababc" =
  Sexp.output_hum Out_channel.stdout [%sexp (classify "bababc" : single)];
  [%expect {| ((two true) (three true)) |}]
;;

let%expect_test "abbcde" =
  Sexp.output_hum Out_channel.stdout [%sexp (classify "abbcde" : single)];
  [%expect {| ((two true) (three false)) |}]
;;

let%expect_test "abcccd" =
  Sexp.output_hum Out_channel.stdout [%sexp (classify "abcccd" : single)];
  [%expect {| ((two false) (three true)) |}]
;;

let%expect_test "aabcdd" =
  Sexp.output_hum Out_channel.stdout [%sexp (classify "aabcdd" : single)];
  [%expect {| ((two true) (three false)) |}]
;;

let%expect_test "abcdee" =
  Sexp.output_hum Out_channel.stdout [%sexp (classify "abcdee" : single)];
  [%expect {| ((two true) (three false)) |}]
;;

let%expect_test "ababab" =
  Sexp.output_hum Out_channel.stdout [%sexp (classify "ababab" : single)];
  [%expect {| ((two false) (three true)) |}]
;;

let add_to_total { twos; threes } box_id =
  let { two; three } = classify box_id in
  { twos   = twos   + if two   then 1 else 0
  ; threes = threes + if three then 1 else 0
  }
;;

let get_totals = List.fold ~init:{ twos = 0; threes = 0 } ~f:add_to_total

let examples =
  [ "abcdef"
  ; "bababc"
  ; "abbcde"
  ; "abcccd"
  ; "aabcdd"
  ; "abcdee"
  ; "ababab"
  ]
;;

let%expect_test "totals for example" =
  Sexp.output_hum Out_channel.stdout [%sexp (get_totals examples : t)];
  [%expect {| ((twos 4) (threes 3)) |}]
;;

let run box_ids = let { twos; threes } = get_totals box_ids in twos * threes

let%expect_test "run example" =
  Sexp.output_hum Out_channel.stdout [%sexp (run examples : int)];
  [%expect {| 12 |}]
;;


