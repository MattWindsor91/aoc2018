(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

let with_box_ids box_ids =
  let part_one_result = Part1.run box_ids in
  Out_channel.printf "Part 1: %d\n" part_one_result;
  let part_two_result = Part2.run box_ids in
  Out_channel.printf "Part 2: %s\n" part_two_result;
;;

let get_box_ids file = file |> In_channel.read_lines

let run_exn file () = let box_ids = get_box_ids file in with_box_ids box_ids
let run file = Or_error.try_with (run_exn file)
