(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

type t =
  | Too_many
  | Equal_so_far of int
  | Differ_at of int
[@@deriving sexp]
;;

let ham_pairwise state chr1 chr2 =
  match state with
  | Too_many -> Too_many
  | Equal_so_far num_seen when not (Char.equal chr1 chr2) -> Differ_at num_seen
  | Equal_so_far num_seen -> Equal_so_far (num_seen + 1)
  | Differ_at _ when not (Char.equal chr1 chr2) -> Too_many
  | Differ_at _ as x -> x
;;

(** [ham str1 str2] checks to see if [str1] and [str2] have a Hamming distance
    of 1.  If so, it returns [Differ_at pos] where [pos] is the position of
    the different character; else, it returns [To_many] or [Equal_so_far]. *)
let ham str1 str2 =
  List.fold2_exn (String.to_list str1) (String.to_list str2)
    ~init:(Equal_so_far 0)
    ~f:ham_pairwise
;;

let%expect_test "abcde/axcye: too many" =
  Sexp.output_hum Out_channel.stdout [%sexp (ham "abcde" "axcye" : t)];
  [%expect {| Too_many |}]
;;

let%expect_test "fghij/fguij: just right" =
  Sexp.output_hum Out_channel.stdout [%sexp (ham "fghij" "fguij" : t)];
  [%expect {| (Differ_at 2) |}]
;;

(** [sliced_ham str1 str2] checks to see if [str1] and [str2] have a Hamming
    distance of 1; if they do, it removes the changed character and returns
    the result as [Some result]. *)
let sliced_ham str1 str2 =
  match ham str1 str2 with
  | Too_many | Equal_so_far _ -> None
  | Differ_at x ->
    Some
      (String.concat [ String.prefix str1 x; String.drop_prefix str1 (x + 1) ])
;;

let%expect_test "abcde/axcye sliced: too many" =
  Sexp.output_hum Out_channel.stdout [%sexp (sliced_ham "abcde" "axcye" : string option)];
  [%expect {| () |}]
;;

let%expect_test "fghij/fguij sliced: just right" =
  Sexp.output_hum Out_channel.stdout [%sexp (sliced_ham "fghij" "fguij" : string option)];
  [%expect {| (fgij) |}]
;;

(** [ham_list strs] exhaustively tests each string pair in [strs] until it
    finds one with a Hamming distance of 1; if it finds one, it removes the
    changed character. *)
let rec ham_list = function
  | [] | [_] -> None
  | str1 :: str2s ->
    match List.find_map str2s ~f:(sliced_ham str1) with
    | None -> ham_list str2s
    | result -> result
;;

let examples =
  [ "abcde"
  ; "fghij"
  ; "klmno"
  ; "pqrst"
  ; "fguij"
  ; "axcye"
  ; "wvxyz"
  ]
;;

let%expect_test "example" =
  Sexp.output_hum Out_channel.stdout [%sexp (ham_list examples : string option)];
  [%expect {| (fgij) |}]
;;

let run box_ids = Option.value (ham_list box_ids) ~default:"(NONE FOUND)"
