(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

(** [run box_ids] runs AOC2018 day 2 part 2 on box IDs [box_ids]. *)
val run : string list -> string
