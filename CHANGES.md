2018-12-05
----------

- Day 4, part 2.
- Day 5, part 1.
- Minor comment fixes.
- Added a `dayN` module; this is a template for use in creating further
  days.

2018-12-04
----------

- Fixed incorrect copyright dates.  Oops.
- Day 4, part 1.

2018-12-03
----------

- Day 3, parts 1 and 2 (very inefficiently).

2018-12-02
----------

- Replaced `run1` with a new `aoc` binary (to use, pass the day as an argument).
- Day 2, parts 1 and 2 (inefficiently).

2018-12-01
----------

- Initial commit.
- Day 1, parts 1 and 2.
