# Advent of Code 2018 solutions

Here are my solutions to the Advent of Code 2018 problems, in OCaml.

See `CHANGES.md` to see which days are covered.

## Prerequisites

- OCaml v4.07+
- dune v1.5+
- core_kernel v0.11+

## How to run

- For each day `N`, put the day's input in `dayN/input`.
- Run `dune exec bin/aoc.exe N`, where `N` is the day you want to run.

## License

WTFPL (effectively public domain).  See `LICENSE.md`.
