(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

let with_deltas deltas =
  let part_one_result = Part1.run deltas in
  Out_channel.printf "Part 1: %d\n" part_one_result;
  let part_two_result = Part2.run deltas in
  Out_channel.printf "Part 2: %d\n" part_two_result;
;;

let get_deltas file = file |> In_channel.read_lines |> List.map ~f:Int.of_string

let run_exn file () = let deltas = get_deltas file in with_deltas deltas
let run file = Or_error.try_with (run_exn file)
