(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

let run deltas = List.fold_left deltas ~init:0 ~f:(+)

let%expect_test "example 1" =
  Out_channel.printf "%d\n" (run [1; -2; 3; 1]);
  [%expect {| 3 |}]
;;

let%expect_test "example 2" =
  Out_channel.printf "%d\n" (run [1; 1; 1]);
  [%expect {| 3 |}]
;;

let%expect_test "example 3" =
  Out_channel.printf "%d\n" (run [1; 1; -2]);
  [%expect {| 0 |}]
;;

let%expect_test "example 4" =
  Out_channel.printf "%d\n" (run [-1; -2; -3]);
  [%expect {| -6 |}]
;;
