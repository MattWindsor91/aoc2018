(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

type t = { current : int; seen_already : Int.Set.t }

let adjust_frequency { current; seen_already } delta =
  let next = current + delta in
  if Int.Set.mem seen_already next
  then Base.Continue_or_stop.Stop next
  else Continue
      { current = next; seen_already = Int.Set.add seen_already next }
;;

let run deltas =
  let infinite_deltas = Sequence.cycle_list_exn deltas in
  Sequence.fold_until infinite_deltas
    ~init:{ current = 0; seen_already = Int.Set.singleton 0 }
    ~f:adjust_frequency
    ~finish:(fun _ -> assert false) (* Infinite sequence! *)
;;

let%expect_test "example 1" =
  Out_channel.printf "%d\n" (run [1; -2; 3; 1]);
  [%expect {| 2 |}]
;;

let%expect_test "example 2" =
  Out_channel.printf "%d\n" (run [1; -1]);
  [%expect {| 0 |}]
;;

let%expect_test "example 3" =
  Out_channel.printf "%d\n" (run [3; 3; 4; -2; -4]);
  [%expect {| 10 |}]
;;

let%expect_test "example 4" =
  Out_channel.printf "%d\n" (run [-6; 3; 8; 5; -6]);
  [%expect {| 5 |}]
;;

let%expect_test "example 5" =
  Out_channel.printf "%d\n" (run [7; 7; -2; -7; -4]);
  [%expect {| 14 |}]
;;
