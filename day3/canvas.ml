(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

type t =
  { start_x : int
  ; start_y : int
  ; data    : Int.Set.t array array
  }
[@@deriving fields]
;;

let make ~x ~y ~x2 ~y2 =
  let dimx = (x2 - x) + 1 in
  let dimy = (y2 - y) + 1 in
  { start_x   = x
  ; start_y   = y
  ; data      = Array.make_matrix ~dimx ~dimy (Int.Set.empty)
  }
;;

let add_claim_mut canvas claim =
  let eff_x  = Claim.x  claim - canvas.start_x in
  let eff_y  = Claim.y  claim - canvas.start_y in
  let eff_x2 = Claim.x2 claim - canvas.start_x in
  let eff_y2 = Claim.y2 claim - canvas.start_y in
  for x = eff_x to eff_x2 do
    for y = eff_y to eff_y2 do
      canvas.data.(x).(y) <- Int.Set.add (canvas.data.(x).(y)) (Claim.id claim)
    done
  done
;;

let of_claims claims =
  let (x, y, x2, y2) = Claim.bounding_box claims in
  let canvas = make ~x ~y ~x2 ~y2 in
  List.iter ~f:(add_claim_mut canvas) claims;
  canvas
;;
