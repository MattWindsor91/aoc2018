(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

let run claims =
  let canvas = Canvas.of_claims claims in
  Array.fold (Canvas.data canvas)
    ~init:0 ~f:(fun k arr ->
        k + Array.count arr ~f:(fun set -> 2 <= Int.Set.length set)
      )
;;

let parse_and_run claim_strs =
  Or_error.(combine_errors (List.map ~f:Claim.of_string claim_strs) >>| run)
;;

let%expect_test "run: example" =
  let example =
    [ "#1 @ 1,3: 4x4"
    ; "#2 @ 3,1: 4x4"
    ; "#3 @ 5,5: 2x2"
    ]
  in
  Sexp.output_hum Out_channel.stdout
    [%sexp (parse_and_run example : int Or_error.t) ];
  [%expect {| (Ok 4) |}]
;;
