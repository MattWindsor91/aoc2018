(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

type t =
  { id     : int
  ; x      : int
  ; y      : int
  ; width  : int
  ; height : int
  }
[@@deriving sexp, fields]
;;

let of_string_exn str =
  Caml.Scanf.sscanf str "#%d @ %d,%d: %dx%d"
    (fun id x y width height -> Fields.create ~id ~x ~y ~width ~height)
;;

let of_string str = Or_error.try_with (fun () -> of_string_exn str)

let%expect_test "of_string: first example" =
  Sexp.output_hum Out_channel.stdout
    [%sexp (of_string "#123 @ 3,2: 5x4" : t Or_error.t)];
  [%expect {| (Ok ((id 123) (x 3) (y 2) (width 5) (height 4))) |}]
;;

let x2 claim = (x claim + width claim) - 1
let y2 claim = (y claim + height claim) - 1

let bounding_box = function
  | [] -> (0, 0, 0, 0)
  | claim :: claims ->
    let init = x claim, y claim, x2 claim, y2 claim in
    List.fold_right claims ~init
      ~f:(fun next_claim (old_x, old_y, old_x2, old_y2) ->
          ( Int.min old_x  (x  next_claim)
          , Int.min old_y  (y  next_claim)
          , Int.max old_x2 (x2 next_claim)
          , Int.max old_y2 (y2 next_claim)
          )
        )
;;
