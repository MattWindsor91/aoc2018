(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

(** [t] is the opaque, mutable type of canvases. *)
type t

(** [data canvas] exposes the canvas's matrix of ID sets. *)
val data : t -> Int.Set.t array array

(** [make ~x ~y ~x2 ~y2] makes a canvas whose claimable area is bounded at
    the left by [x], the top by [y], the right by [x2], and the bottom by
    [y2]. *)
val make
  :  x:int
  -> y:int
  -> x2:int
  -> y2:int
  -> t
;;

(** [add_claim_mut canvas claim] adds [claim] to [canvas] in-place. *)
val add_claim_mut : t -> Claim.t -> unit

(** [of_claims claims] creates a canvas containing the claims in [claims]. *)
val of_claims : Claim.t list -> t
