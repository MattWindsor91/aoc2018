(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)


open Base

(** [t] is the opaque type of claims. *)
type t

(** [id claim] gets the ID of [claim]. *)
val id : t -> int

(** [x claim] gets the X co-ordinate of the left side of [claim]. *)
val x : t -> int

(** [y claim] gets the Y co-ordinate of the top side of [claim]. *)
val y : t -> int

(** [x2 claim] gets the X co-ordinate of the right side of [claim]. *)
val x2 : t -> int

(** [y2 claim] gets the Y co-ordinate of the bottom side of [claim]. *)
val y2 : t -> int

(** [of_string str] tries to parse a string as a claim. *)
val of_string : string -> t Or_error.t

(** [bounding_box claims] generates a (left, top, right, bottom) bounding box
    that will hold all claims in [claims]. *)
val bounding_box : t list -> (int * int * int * int)
