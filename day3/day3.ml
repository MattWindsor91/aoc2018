(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

let with_claims claims =
  let part_one_result = Part1.run claims in
  Out_channel.printf "Part 1: %d\n" part_one_result;
  let part_two_result = Part2.run claims in
  Out_channel.printf "Part 2: %a\n"
    Sexp.output_hum [%sexp (part_two_result : Int.Set.t)]
;;

let get_claims file =
  Or_error.(
    try_with (fun () -> In_channel.read_lines file)
    >>= (fun ls -> ls |> List.map ~f:Claim.of_string |> Or_error.combine_errors)
  )
;;

let run file = Or_error.(file |> get_claims >>| with_claims)
