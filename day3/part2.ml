(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

let id_set claims =
  claims
  |> List.map ~f:Claim.id
  |> Int.Set.of_list
;;

let remove_overlaps main_set cell_set =
  Int.Set.(
    if 2 <= length cell_set
    then Int.Set.diff main_set cell_set
    else main_set
  )
;;

let run claims =
  let canvas = Canvas.of_claims claims in
  Array.fold (Canvas.data canvas) ~init:(id_set claims)
    ~f:(fun set -> Array.fold ~f:remove_overlaps ~init:set)
;;

let parse_and_run claim_strs =
  Or_error.(combine_errors (List.map ~f:Claim.of_string claim_strs) >>| run)
;;

let%expect_test "run: example" =
  let example =
    [ "#1 @ 1,3: 4x4"
    ; "#2 @ 3,1: 4x4"
    ; "#3 @ 5,5: 2x2"
    ]
  in
  Sexp.output_hum Out_channel.stdout
    [%sexp (parse_and_run example : Int.Set.t Or_error.t) ];
  [%expect {| (Ok (3)) |}]
;;
