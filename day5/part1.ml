(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

let example = "dabAcCaCBAcCcaDA"

let units_react c1 c2 =
  Char.(
    match is_uppercase c1, is_uppercase c2 with
    | true, true | false, false -> false
    | _, _ -> lowercase c1 = lowercase c2
  )
;;

let no_unit = ' '

let valid_unit = Char.is_alpha

let react get initial_size =
  let buf = Buffer.create initial_size in

  let proc_reaction nunits =
    let nunits' = nunits - 1 in
    Buffer.truncate buf nunits';
    let last_unit' =
      if nunits' = 0 then no_unit else Buffer.nth buf (nunits' - 1)
    in (last_unit', nunits')
  in

  let rec mu last_unit nunits = function
    | None -> nunits
    | Some this_unit ->
      let last_unit', nunits' = (
        if not (valid_unit this_unit) then (last_unit, nunits)
        else if units_react last_unit this_unit then proc_reaction nunits
        else (Buffer.add_char buf this_unit; (this_unit, nunits + 1))
      ) in mu last_unit' nunits' (get ())
  in
  let nunits = mu no_unit 0 (get ()) in
  (nunits, Buffer.contents buf)
;;

let react_string str =
  let i = ref 0 in
  let strlen = String.length str in
  let get () =
    if !i < strlen then (let c = String.get str !i in incr i; Some c) else None
  in react get strlen
;;

let%expect_test "react_string: example 1" =
  let n, remains = react_string "aA" in
  printf "%s (%d)" remains n;
  [%expect {| (0) |}]
;;

let%expect_test "react_string: example 2" =
  let n, remains = react_string "abBA" in
  printf "%s (%d)" remains n;
  [%expect {| (0) |}]
;;

let%expect_test "react_string: example 3" =
  let n, remains = react_string "abAB" in
  printf "%s (%d)" remains n;
  [%expect {| abAB (4) |}]
;;

let%expect_test "react_string: example 4" =
  let n, remains = react_string "aabAAB" in
  printf "%s (%d)" remains n;
  [%expect {| aabAAB (6) |}]
;;

let%expect_test "react_string: main example" =
  let n, remains = react_string example in
  printf "%s (%d)" remains n;
  [%expect {| dabCBAcaDA (10) |}]
;;

let react_ic ic = let get () = In_channel.input_char ic in react get 1024

let run fname =
  Or_error.(
    try_with (fun () -> In_channel.with_file fname ~f:react_ic)
    >>| fst
  )
;;
