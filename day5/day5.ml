(* Copyright © 2018 Matt Windsor <nihilistmatt@gmail.com>
   This work is free. You can redistribute it and/or modify it under the
   terms of the Do What The Fuck You Want To Public License, Version 2,
   as published by Sam Hocevar. See the COPYING file for more details. *)

open Core_kernel

let run file =
  let open Or_error.Let_syntax in
  let%map part_one_result = Part1.run file in
  Out_channel.printf "Part 1: %d\n" part_one_result
;;
